﻿using System.Runtime.InteropServices;
using PowerAPI;

namespace PowerService
{
    [ComVisible(true)]
    [Guid("BFDBD499-CAE5-430C-9B62-91860AE682AB")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPowerService
    {
        ulong GetLastSleepTime();
        ulong GetLastWakeTime();
        SystemBatteryState GetSystemBatteryState();
        SystemPowerInformation GetSystemPowerInformation();
        uint GetSystemBatteryStateEstimatedTime();
        uint GetSystemPowerInformationTimeRemaining();
        void ReserveHibernationFile();
        void RemoveHibernationFile();
        void Sleep();
        void Hibernate();
    }
}