﻿using System;
using System.Runtime.InteropServices;
using PowerAPI;

namespace PowerService
{
    [ComVisible(true)]
    [Guid("C0206BBC-4B0A-47FF-9805-A3A4605E631B")]
    [ClassInterface(ClassInterfaceType.AutoDispatch)]
    public class PowerService : IPowerService
    {
        public ulong GetLastSleepTime()
        {
            ulong lastSleepTime;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.LastSleepTime,
                IntPtr.Zero,
                0,
                out lastSleepTime,
                Marshal.SizeOf(typeof(ulong))
                );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
            return lastSleepTime;
        }

        public ulong GetLastWakeTime()
        {
            ulong lastWakeTime;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.LastWakeTime,
                IntPtr.Zero,
                0,
                out lastWakeTime,
                Marshal.SizeOf(typeof(ulong))
                );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
            return lastWakeTime;
        }

        public SystemBatteryState GetSystemBatteryState()
        {
            SystemBatteryState sbs;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.SystemBatteryState,
                IntPtr.Zero,
                0,
                out sbs,
                Marshal.SizeOf(typeof(SystemBatteryState))
                );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
            return sbs;
        }

        public SystemPowerInformation GetSystemPowerInformation()
        {
            SystemPowerInformation spi;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.SystemPowerInformation,
                IntPtr.Zero,
                0,
                out spi,
                Marshal.SizeOf(typeof(SystemPowerInformation))
            );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
            return spi;
        }

        public uint GetSystemBatteryStateEstimatedTime()
        {
            return GetSystemBatteryState().EstimatedTime;
        }

        public uint GetSystemPowerInformationTimeRemaining()
        {
            return GetSystemPowerInformation().TimeRemaining;
        }

        public void ReserveHibernationFile()
        {
            ulong temp;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.SystemReserveHiberFile,
                new IntPtr(1),
                Marshal.SizeOf(typeof(IntPtr)),
                out temp,
                Marshal.SizeOf(typeof(ulong))
                );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
        }

        public void RemoveHibernationFile()
        {
            ulong temp;
            var retval = PowerApi.CallNtPowerInformation(
                InformationLevel.SystemReserveHiberFile,
                new IntPtr(0),
                Marshal.SizeOf(typeof(IntPtr)),
                out temp,
                Marshal.SizeOf(typeof(ulong))
                );
            if (retval != NtStatus.Success)
            {
                throw new COMException(retval.ToString());
            }
        }

        public void Sleep()
        {
            var retval = PowerApi.SetSuspendState(false, false, false);
            if (!retval)
            {
                throw new COMException();
            }
        }

        public void Hibernate()
        {
            var retval = PowerApi.SetSuspendState(true, false, false);
            if (!retval)
            {
                throw new COMException();
            }
        }
    }
}
