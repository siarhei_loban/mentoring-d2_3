﻿using System.Runtime.InteropServices;

namespace PowerAPI
{
    public struct SystemPowerInformation
    {
        public uint MaxIdlenessAllowed;
        public uint Idleness;
        public uint TimeRemaining;
        [MarshalAs(UnmanagedType.I1)]
        public bool CoolingMode;
    }
}