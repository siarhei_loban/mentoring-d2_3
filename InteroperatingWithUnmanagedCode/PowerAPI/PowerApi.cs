﻿using System;
using System.Runtime.InteropServices;

namespace PowerAPI
{
    public static class PowerApi
    {
        [DllImport("powrprof.dll")]
        public static extern NtStatus CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out ulong lpOutputBuffer,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll")]
        public static extern NtStatus CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out SystemBatteryState lpOutputBuffer,
            int nOutputBufferSize
        );

        [DllImport("powrprof.dll")]
        public static extern NtStatus CallNtPowerInformation(
            InformationLevel informationLevel,
            IntPtr lpInputBuffer,
            int nInputBufferSize,
            out SystemPowerInformation lpOutputBuffer,
            int nOutputBufferSize
        );

        [DllImport("Powrprof.dll", SetLastError = true)]
        public static extern bool SetSuspendState(
            bool hibernate, 
            bool forceCritical, 
            bool disableWakeEvent
        );
    }
}
