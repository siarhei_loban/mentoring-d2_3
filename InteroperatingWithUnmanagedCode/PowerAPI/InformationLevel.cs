﻿namespace PowerAPI
{
    public enum InformationLevel : uint
    {
        SystemBatteryState = 5,
        SystemReserveHiberFile = 10,
        SystemPowerInformation = 12,
        LastWakeTime = 14,
        LastSleepTime = 15
    }
}