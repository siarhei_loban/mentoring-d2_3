set powerService = CreateObject("PowerService.PowerService")
lastSleepTime = powerService.GetLastSleepTime
lastWakeTime = powerService.GetLastWakeTime
systemBatteryStateEstimatedTime = powerService.GetSystemBatteryStateEstimatedTime
systemPowerInformationTimeRemaining = powerService.GetSystemPowerInformationTimeRemaining

MsgBox lastSleepTime, 0, "Last sleep time"
MsgBox lastWakeTime, 0, "Last wake time"
MsgBox systemBatteryStateEstimatedTime, 0, "System Battery State: Estimated Time"
MsgBox systemPowerInformationTimeRemaining, 0, "System Power Information: Time Remaining"