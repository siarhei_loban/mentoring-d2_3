﻿namespace AspectOrientedProgramming
{
    class ImageFile
    {
        public string Path { get; set; }
        public int Number { get; set; }
    }
}
