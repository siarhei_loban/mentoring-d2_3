﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Topshelf;

namespace AspectOrientedProgramming
{
    class Program
    {
        static void Main(string[] args)
        {
            var appDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            var inDir = Path.Combine(appDir, ConfigurationManager.AppSettings["InDir"]);
            var outDir = Path.Combine(appDir, ConfigurationManager.AppSettings["OutDir"]);
            var errorDir = Path.Combine(appDir, ConfigurationManager.AppSettings["ErrorDir"]);
            var nextDocumentTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["NextDocumentTimeOut"]);
            var imageFileRegex = ConfigurationManager.AppSettings["ImageFileRegex"];

            var fileTarget = new FileTarget
            {
                Name = "Default",
                FileName = Path.Combine(appDir, "log.txt"),
                Layout = "${date} ${message} ${onexception:inner=${exception:format=toString}}"
            };
            var loggingConfiguration = new LoggingConfiguration();
            loggingConfiguration.AddTarget(fileTarget);
            loggingConfiguration.AddRuleForAllLevels(fileTarget);
            var logFactory = new LogFactory(loggingConfiguration);

            HostFactory.Run(hostConfig =>
            {
                hostConfig.Service<ImagesToPdfService>(settings =>
                {
                    settings.ConstructUsing(() => new ImagesToPdfService(inDir, outDir, errorDir, nextDocumentTimeOut, imageFileRegex));
                    settings.WhenStarted((tc, hostControl) => tc.Start(hostControl));
                    settings.WhenStopped((tc, hostControl) => tc.Stop(hostControl));
                }).UseNLog(logFactory);
                hostConfig.RunAsLocalService();
            });
        }
    }
}
