﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using Topshelf;
using ZXing;
using Timer = System.Timers.Timer;

namespace AspectOrientedProgramming
{
    class ImagesToPdfService : ServiceControl
    {
        private readonly string _inDir;
        private readonly string _outDir;
        private readonly string _errorDir;
        private readonly int _nextDocumentTimeOut;
        private readonly string _imageFileRegex;
        private readonly BarcodeReader _reader;
        private readonly FileSystemWatcher _watcher;
        private static Timer _timer;
        private readonly Thread _workThread;
        private readonly ManualResetEvent _stopWorkEvent;
        private readonly AutoResetEvent _scanBunchCompletedEvent;

        public ImagesToPdfService(string inDir, string outDir, string errorDir, int nextDocumentTimeOut, string imageFileRegex)
        {
            _inDir = inDir;
            _outDir = outDir;
            _errorDir = errorDir;
            _nextDocumentTimeOut = nextDocumentTimeOut;
            _imageFileRegex = imageFileRegex;

            CreateDirectoryIfNotExists(_inDir);
            CreateDirectoryIfNotExists(_outDir);
            CreateDirectoryIfNotExists(_errorDir);

            _reader = new BarcodeReader { AutoRotate = true };

            _watcher = new FileSystemWatcher(_inDir);
            _watcher.Created += Watcher_Created;

            _timer = new Timer
            {
                Interval = _nextDocumentTimeOut,
                AutoReset = false
            };
            _timer.Elapsed += Timer_Elapsed;

            _workThread = new Thread(StartProcessing);
            _workThread.Start();

            _stopWorkEvent = new ManualResetEvent(false);
            _scanBunchCompletedEvent = new AutoResetEvent(false);
        }

        private void StartProcessing()
        {
            do
            {

                var imageSequence = new ConcurrentBag<ImageFile>();
                foreach (var file in Directory.EnumerateFiles(_inDir))
                {
                    if (_stopWorkEvent.WaitOne(TimeSpan.Zero))
                    {
                        return;
                    }
                    var fileName = Path.GetFileName(file);
                    var regex = new Regex(_imageFileRegex, RegexOptions.IgnoreCase);
                    if (regex.IsMatch(fileName))
                    {
                        var fileNumber = GetFileNumber(Path.GetFileNameWithoutExtension(file));
                        imageSequence.Add(new ImageFile
                        {
                            Path = file,
                            Number = fileNumber
                        });
                    }
                }

                if (imageSequence.Any(x => !IsValidImage(x.Path)))
                {
                    foreach (var image in imageSequence)
                    {
                        File.Move(image.Path, _errorDir + Path.GetFileName(image.Path));
                    }
                    return;
                }
                if (imageSequence.Count > 1)
                {
                    foreach (var image in imageSequence)
                    {
                        if (imageSequence.All(x => x.Number != image.Number + 1 && x.Number != image.Number - 1))
                        {
                            ImageFile result;
                            imageSequence.TryTake(out result);
                        }
                        if (imageSequence.Count == 1)
                        {
                            break;
                        }
                    }
                }

                CreatePdf(imageSequence.OrderBy(x => x.Number).ToList());

            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _scanBunchCompletedEvent }, 5000) != 0);
        }

        private bool CreatePdf(List<ImageFile> imageSequence)
        {
            if (imageSequence.Count == 0)
            {
                return false;
            }
            var document = new Document();
            var section = document.AddSection();

            foreach (var file in imageSequence)
            {
                if (TryOpen(file.Path, 3))
                {
                    var image = section.AddImage(file.Path);
                    image.Height = document.DefaultPageSetup.PageHeight;
                    image.Width = document.DefaultPageSetup.PageWidth;
                }
                else
                {
                    return false;
                }
            }
            var render = new PdfDocumentRenderer { Document = document };
            render.RenderDocument();

            var fileNameToSave = Path.GetFileNameWithoutExtension(imageSequence.First().Path) + ".pdf";

            var existingFile =
                Directory.GetFiles(_outDir, fileNameToSave, SearchOption.TopDirectoryOnly).FirstOrDefault();
            if (existingFile != null)
            {
                fileNameToSave = Guid.NewGuid() + Path.GetExtension(fileNameToSave);
            }

            var savePath = Path.Combine(_outDir, fileNameToSave);
            render.Save(savePath);
            foreach (var file in imageSequence)
            {
                File.Delete(file.Path);
            }

            return true;
        }

        private bool TryOpen(string fileName, int tryCount)
        {
            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    file.Close();
                    return true;
                }
                catch (IOException ex)
                {
                    Thread.Sleep(5000);
                }
            }

            return false;
        }

        private bool IsValidImage(string filename)
        {
            try
            {
                Image img;
                using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    img = Image.FromStream(stream);
                }
                return img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch (OutOfMemoryException)
            {
                return false;
            }
        }

        private int GetFileNumber(string fileName)
        {
            var tobesearched = "img_";
            int ix = fileName.IndexOf(tobesearched, StringComparison.InvariantCultureIgnoreCase);
            var fileNumber = fileName.Substring(ix + tobesearched.Length);
            return Convert.ToInt32(fileNumber);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _scanBunchCompletedEvent.Set();
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            _scanBunchCompletedEvent.Set();
        }

        private void CreateDirectoryIfNotExists(string dir)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public bool Start(HostControl hostControl)
        {
            _watcher.EnableRaisingEvents = true;
            _watcher.NotifyFilter = NotifyFilters.LastWrite;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _timer.Start();
            return true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Start();
            }
        }

        public bool Stop(HostControl hostControl)
        {
            _watcher.EnableRaisingEvents = false;
            _stopWorkEvent.Set();
            _workThread.Join();
            _timer.Stop();
            return true;
        }
    }
}
