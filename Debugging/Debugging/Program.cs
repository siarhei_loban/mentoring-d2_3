﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Debugging
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GenerateKey());
            //Form f = new Form1();
            //f.ShowDialog();
            Console.ReadLine();
        }

        static string GenerateKey()
        {
            var bytesOfPhysicalAddress = GetBytesOfPhysicalAddress();
            var bytesOfCurrentDate = GetBytesOfCurrentDate();
            var partsOfKey = GetPartsOfKey(bytesOfPhysicalAddress, bytesOfCurrentDate);
            var key = GetKey(partsOfKey);
            return key;
        }

        static IEnumerable<byte> GetBytesOfPhysicalAddress()
        {
            var networkInterface = NetworkInterface.GetAllNetworkInterfaces().FirstOrDefault<NetworkInterface>();
            return networkInterface.GetPhysicalAddress().GetAddressBytes();
        }

        static IEnumerable<byte> GetBytesOfCurrentDate()
        {
            return BitConverter.GetBytes(DateTime.Now.Date.ToBinary());
        }

        static int CurrentDateDoXorPhysicalAddressByteAndCurrentDateByte(byte a, byte d)
        {
            return (int) (a ^ d);
        }

        static int Mult10IfLessThen999(int a)
        {
            return a <= 999 ? a * 10 : a;
        }

        static IEnumerable<int> GetPartsOfKey(IEnumerable<byte> bytesOfPhysicalAddress, IEnumerable<byte> bytesOfCurrentDate)
        {
            var arrayOfAddress = bytesOfPhysicalAddress.ToArray();
            var arrayOfDate = bytesOfCurrentDate.ToArray();
            var result = new List<int>();
            for (var i = 0; i < arrayOfAddress.Length; i++)
            {
                var xor = CurrentDateDoXorPhysicalAddressByteAndCurrentDateByte(arrayOfAddress[i], arrayOfDate[i]);
                result.Add(Mult10IfLessThen999((xor)));
            }
            return result;
        }

        static string GetKey(IEnumerable<int> partsOfKey)
        {
            return String.Join("-", partsOfKey);
        }
    }
}
