﻿using System;

namespace ProfileSample.Models
{
    public class ImageModel
    {
        public string Name { get; set; }

        public byte[] Data { get; set; }

        public string ImageUrl
        {
            get { return $"data:image/jpg;base64,{Convert.ToBase64String(Data)}"; }
        }
    }
}