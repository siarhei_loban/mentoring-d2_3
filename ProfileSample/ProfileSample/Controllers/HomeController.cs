﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using ProfileSample.DAL;
using ProfileSample.Models;

namespace ProfileSample.Controllers
{
    public class HomeController : Controller
    {
        private readonly ProfileSampleEntities _context;
        public HomeController()
        {
            _context = new ProfileSampleEntities();
        }

        public ActionResult Index()
        {
            var model = _context
                .ImgSources
                .Take(20)
                .Select(x => new ImageModel()
                {
                    Name = x.Name,
                    Data = x.Data
                }).ToList();

            return View(model);
        }

        public ActionResult Convert()
        {
            var files = Directory.GetFiles(Server.MapPath("~/Content/Img"), "*.jpg");

            foreach (var file in files)
            {
                using (var stream = new FileStream(file, FileMode.Open))
                {
                    byte[] buff = new byte[stream.Length];

                    stream.Read(buff, 0, (int) stream.Length);

                    var entity = new ImgSource()
                    {
                        Name = Path.GetFileName(file),
                        Data = buff,
                    };

                    _context.ImgSources.Add(entity);
                    _context.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }
    }
}