﻿using System;
using System.Collections.Generic;

namespace CentralServer
{
    public class PartialFile
    {
        public PartialFile()
        {
            PartialFileItems = new List<PartialFileItem>();
        }
        public Guid Sequence { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public List<PartialFileItem> PartialFileItems { get; set; }
        public bool ReadyToBeSaved
        {
            get { return Size == PartialFileItems.Count; }
        }
    }
}
