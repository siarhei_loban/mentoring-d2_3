﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Threading;
using Common;

namespace CentralServer
{
    class Program
    {
        private static string fileQueuePath;
        private static string outDir;
        private static string configQueuePath;
        private static string configQueueMulticastAddress;
        static void Main(string[] args)
        {
            fileQueuePath = ConfigurationManager.AppSettings["FileQueuePath"];
            configQueuePath = ConfigurationManager.AppSettings["ConfigQueuePath"];
            configQueueMulticastAddress = ConfigurationManager.AppSettings["ConfigQueueMulticastAddress"];
            var appDir = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName);
            outDir = Path.Combine(appDir, ConfigurationManager.AppSettings["OutDir"]);
            CreateDirectoryIfNotExists(outDir);

            if (!MessageQueue.Exists(fileQueuePath))
            {
                MessageQueue.Create(fileQueuePath);
            }

            var threadForForReceivingFiles = new Thread(WorkForReceivingFiles);
            var threadForSendingConfig = new Thread(WorkForSendingConfig);
            threadForForReceivingFiles.Start();
            threadForSendingConfig.Start();
            threadForSendingConfig.Join();
            threadForForReceivingFiles.Join();
        }
        private static void CreateDirectoryIfNotExists(string dir)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        private static void WorkForSendingConfig()
        {
            using (var queue = new MessageQueue(configQueuePath))
            {
                queue.MulticastAddress = configQueueMulticastAddress;
                queue.Formatter = new BinaryMessageFormatter();
                while (true)
                {
                    try
                    {
                        Console.WriteLine("NextDocumentTimeOut: ");
                        var nextDocumentTimeOut = Convert.ToInt32(Console.ReadLine());
                        var config = new Config
                        {
                            NextDocumentTimeOut = nextDocumentTimeOut
                        };
                        using (var message = new Message())
                        {
                            message.Formatter = new BinaryMessageFormatter();
                            message.Recoverable = true;
                            message.Body = config;
                            message.Label = "Config";
                            queue.Send(message);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
        }

        private static void WorkForReceivingFiles()
        {
            using (var queue = new MessageQueue(fileQueuePath))
            {
                queue.Formatter = new BinaryMessageFormatter();
                var partialFiles = new List<PartialFile>();
                using (var messageEnumerator = queue.GetMessageEnumerator2())
                {
                    while (messageEnumerator.MoveNext(TimeSpan.FromHours(3)))
                    {
                        var partialMessage = (PartialMessage)messageEnumerator.Current.Body;
                        var partialFile = partialFiles.FirstOrDefault(pf => pf.Sequence == partialMessage.Sequence);
                        if (partialFile == null)
                        {
                            partialFile = new PartialFile
                            {
                                Sequence = partialMessage.Sequence,
                                Name = partialMessage.Name,
                                Size = partialMessage.Size,
                                PartialFileItems = new List<PartialFileItem>()
                            };
                            partialFiles.Add(partialFile);
                        }
                        var partialFileItem = new PartialFileItem
                        {
                            Position = partialMessage.Position,
                            MessageId = messageEnumerator.Current.Id
                        };
                        partialFile.PartialFileItems.Add(partialFileItem);
                        if (partialFile.ReadyToBeSaved)
                        {
                            var name = partialFile.Name;
                            var partialFileItems = partialFile.PartialFileItems.OrderBy(pfi => pfi.Position);
                            var bodies = partialFileItems
                                .Select(pfi => ((PartialMessage)queue.ReceiveById(pfi.MessageId).Body).Body).ToList();
                            var memoryStream = new MemoryStream();
                            foreach (var body in bodies)
                            {
                                memoryStream.Write(body, 0, body.Length);
                            }
                            File.WriteAllBytes($"{outDir}\\{name}", memoryStream.ToArray());
                            Console.WriteLine("File {0} was received.");
                        }
                    }
                }
            }
        }
    }
}