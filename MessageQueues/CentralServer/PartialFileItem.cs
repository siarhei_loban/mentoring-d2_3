﻿namespace CentralServer
{
    public class PartialFileItem
    {
        public int Position { get; set; }
        public string MessageId { get; set; }
    }
}
