﻿using System;

namespace Common
{
    [Serializable]
    public class PartialMessage
    {
        public const int BodySize = 3145728;
        public Guid Sequence { get; set; }
        public string Name { get; set; }
        public int Position { get; set; }
        public int Size { get; set; }
        public byte[] Body { get; set; }
    }
}
