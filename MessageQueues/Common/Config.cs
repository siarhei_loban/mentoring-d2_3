﻿using System;

namespace Common
{
    [Serializable]
    public class Config
    {
        public int NextDocumentTimeOut { get; set; }
    }
}
