﻿using System.IO;

namespace InputService
{
    public static class StreamExtension
    {
        public static byte[] GetBytes(this Stream stream)
        {
            if (stream == null || stream.Length == 0)
            {
                return new byte[0];
            }
            var bytes = new byte[stream.Length];
            var numBytesToRead = (int)stream.Length;
            var numBytesRead = 0;
            while (numBytesToRead > 0)
            {
                var n = stream.Read(bytes, numBytesRead, numBytesToRead);
                if (n == 0)
                {
                    break;
                }
                numBytesRead += n;
                numBytesToRead -= n;
            }
            return bytes;
        }
    }
}
