﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Messaging;
using System.Text.RegularExpressions;
using System.Threading;
using System.Timers;
using Common;
using MigraDoc.DocumentObjectModel;
using MigraDoc.Rendering;
using NLog.Time;
using Topshelf;
using ZXing;
using Timer = System.Timers.Timer;

namespace InputService
{
    class ImagesToPdfService : ServiceControl
    {
        private readonly string _inDir;
        private readonly string _errorDir;
        private int _nextDocumentTimeOut;
        private readonly string _imageFileRegex;
        private readonly BarcodeReader _reader;
        private readonly FileSystemWatcher _watcher;
        private static Timer _timer;
        private readonly Thread _workThread;
        private readonly Thread _workThreadForUpdatingConfig;
        private readonly ManualResetEvent _stopWorkEvent;
        private readonly AutoResetEvent _scanBunchCompletedEvent;
        private readonly string _fileQueuePath;
        private readonly string _configQueuePath;
        private readonly string _configQueueMulticastAddress;

        public ImagesToPdfService(string inDir, string errorDir, int nextDocumentTimeOut, string imageFileRegex, string fileQueuePath, string configQueuePath, string configQueueMulticastAddress)
        {
            _inDir = inDir;
            _errorDir = errorDir;
            _nextDocumentTimeOut = nextDocumentTimeOut;
            _imageFileRegex = imageFileRegex;
            _fileQueuePath = fileQueuePath;
            _configQueuePath = configQueuePath;
            _configQueueMulticastAddress = configQueueMulticastAddress;

            CreateDirectoryIfNotExists(_inDir);
            CreateDirectoryIfNotExists(_errorDir);

            _reader = new BarcodeReader { AutoRotate = true };

            _watcher = new FileSystemWatcher(_inDir);
            _watcher.Created += Watcher_Created;

            _timer = new Timer
            {
                Interval = _nextDocumentTimeOut,
                AutoReset = false
            };
            _timer.Elapsed += Timer_Elapsed;

            _workThread = new Thread(StartProcessing);
            _workThread.Start();

            _workThreadForUpdatingConfig = new Thread(UpdateConfig);
            _workThreadForUpdatingConfig.Start();

            _stopWorkEvent = new ManualResetEvent(false);
            _scanBunchCompletedEvent = new AutoResetEvent(false);
        }

        private void StartProcessing()
        {
            do
            {

                var imageSequence = new ConcurrentBag<ImageFile>();
                foreach (var file in Directory.EnumerateFiles(_inDir))
                {
                    if (_stopWorkEvent.WaitOne(TimeSpan.Zero))
                    {
                        return;
                    }
                    var fileName = Path.GetFileName(file);
                    var regex = new Regex(_imageFileRegex, RegexOptions.IgnoreCase);
                    if (regex.IsMatch(fileName))
                    {
                        var fileNumber = GetFileNumber(Path.GetFileNameWithoutExtension(file));
                        imageSequence.Add(new ImageFile
                        {
                            Path = file,
                            Number = fileNumber
                        });
                    }
                }

                if (imageSequence.Any(x => !IsValidImage(x.Path)))
                {
                    foreach (var image in imageSequence)
                    {
                        File.Move(image.Path, _errorDir + Path.GetFileName(image.Path));
                    }
                    return;
                }
                if (imageSequence.Count > 1)
                {
                    foreach (var image in imageSequence)
                    {
                        if (imageSequence.All(x => x.Number != image.Number + 1 && x.Number != image.Number - 1))
                        {
                            ImageFile result;
                            imageSequence.TryTake(out result);
                        }
                        if (imageSequence.Count == 1)
                        {
                            break;
                        }
                    }
                }

                CreatePdf(imageSequence.OrderBy(x => x.Number).ToList());

            } while (WaitHandle.WaitAny(new WaitHandle[] { _stopWorkEvent, _scanBunchCompletedEvent }, 5000) != 0);
        }

        private bool CreatePdf(List<ImageFile> imageSequence)
        {
            if (imageSequence.Count == 0)
            {
                return false;
            }
            var document = new Document();
            var section = document.AddSection();

            foreach (var file in imageSequence)
            {
                if (TryOpen(file.Path, 3))
                {
                    var image = section.AddImage(file.Path);
                    image.Height = document.DefaultPageSetup.PageHeight;
                    image.Width = document.DefaultPageSetup.PageWidth;
                }
                else
                {
                    return false;
                }
            }
            var render = new PdfDocumentRenderer { Document = document };
            render.RenderDocument();

            var fileNameToSave = Path.GetFileNameWithoutExtension(imageSequence.First().Path) + ".pdf";

            using (var memoryStream = new MemoryStream())
            {
                render.Save(memoryStream, false);
                SendFile(memoryStream, fileNameToSave);
            }

            foreach (var file in imageSequence)
            {
                File.Delete(file.Path);
            }

            return true;
        }

        private bool TryOpen(string fileName, int tryCount)
        {
            for (int i = 0; i < tryCount; i++)
            {
                try
                {
                    var file = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                    file.Close();
                    return true;
                }
                catch (IOException ex)
                {
                    Thread.Sleep(5000);
                }
            }

            return false;
        }

        private bool IsValidImage(string filename)
        {
            try
            {
                Image img;
                using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    img = Image.FromStream(stream);
                }
                return img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Jpeg) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Png) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Gif) || img.RawFormat.Equals(System.Drawing.Imaging.ImageFormat.Bmp);
            }
            catch (OutOfMemoryException)
            {
                return false;
            }
        }

        private int GetFileNumber(string fileName)
        {
            var tobesearched = "img_";
            int ix = fileName.IndexOf(tobesearched, StringComparison.InvariantCultureIgnoreCase);
            var fileNumber = fileName.Substring(ix + tobesearched.Length);
            return Convert.ToInt32(fileNumber);
        }

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _scanBunchCompletedEvent.Set();
        }

        private void Watcher_Created(object sender, FileSystemEventArgs e)
        {
            _scanBunchCompletedEvent.Set();
        }

        private void CreateDirectoryIfNotExists(string dir)
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        public bool Start(HostControl hostControl)
        {
            _watcher.EnableRaisingEvents = true;
            _watcher.NotifyFilter = NotifyFilters.LastWrite;
            _watcher.Filter = "*.*";
            _watcher.Changed += OnChanged;
            _timer.Start();
            return true;
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Start();
            }
        }

        public bool Stop(HostControl hostControl)
        {
            _watcher.EnableRaisingEvents = false;
            _stopWorkEvent.Set();
            _workThread.Join();
            _workThreadForUpdatingConfig.Join();
            _timer.Stop();
            return true;
        }

        private bool SendFile(Stream stream, string filename)
        {
            try
            {
                var file = stream.GetBytes();
                var sequence = Guid.NewGuid();
                var maxBodySize = PartialMessage.BodySize;
                var fileSize = file.Length;
                var size = fileSize / maxBodySize + (fileSize % maxBodySize > 0 ? 1 : 0);
                using (var queue = new MessageQueue(_fileQueuePath))
                {
                    queue.Formatter = new BinaryMessageFormatter();
                    for (var position = 0; position < size; position++)
                    {

                        var currentBodySize = (fileSize - position * maxBodySize) > maxBodySize ? maxBodySize : fileSize - position * maxBodySize;
                        var body = new byte[currentBodySize];
                        Array.Copy(file, position * maxBodySize, body, 0, currentBodySize);
                        var partialMessage = new PartialMessage
                        {
                            Name = filename,
                            Sequence = sequence,
                            Position = position,
                            Size = size,
                            Body = body
                        };
                        using (var message = new Message())
                        {
                            message.Recoverable = true;
                            message.Formatter = new BinaryMessageFormatter();
                            message.Label = $"Partial message {partialMessage.Sequence}";
                            message.Body = partialMessage;
                            queue.Send(message);
                        }
                    }
                }
            }
            catch (MessageQueueException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }

        private void UpdateConfig()
        {
            using (var queue = new MessageQueue(_configQueuePath))
            {
                queue.MulticastAddress = _configQueueMulticastAddress;
                queue.Formatter = new BinaryMessageFormatter();
                do
                {
                    try
                    {
                        var message = queue.Receive();
                        if (message == null)
                        {
                            continue;
                        }
                        var config = (Config) message.Body;
                        _nextDocumentTimeOut = config.NextDocumentTimeOut;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                } while (true);
            }
        }
    }
}
