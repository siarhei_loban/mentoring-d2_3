﻿namespace ExpressionTrees_ClassMapper
{
    public class Foo
    {
        public int P1 { get; set; }
        public string P2 { get; set; }
        public double P3 { get; set; }
        public double P4 { get; set; }

        public override string ToString()
        {
            return $"Foo[P1={P1}|P2={P2}|P3={P3}|P4={P4}]";
        }
    }
}
