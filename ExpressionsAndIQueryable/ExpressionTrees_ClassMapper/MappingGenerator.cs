﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTrees_ClassMapper
{
    public class MappingGenerator
    {
        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var source = Expression.Parameter(typeof(TSource), "source");
            var memberBinding = (from sourceProperty in typeof (TSource).GetProperties()
                where sourceProperty.CanRead
                let targetProperty = typeof (TDestination).GetProperty(sourceProperty.Name)
                where targetProperty != null && targetProperty.CanWrite
                where targetProperty.PropertyType.IsAssignableFrom(sourceProperty.PropertyType)
                select Expression.Bind(targetProperty, Expression.Property(source, sourceProperty)))
                .Cast<MemberBinding>()
                .ToList();
            var memberInit = Expression.MemberInit(Expression.New(typeof(TDestination)), memberBinding);
            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(memberInit, source);
            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }
    }
}