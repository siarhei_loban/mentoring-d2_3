﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionTrees_ClassMapper
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            var mapGenerator = new MappingGenerator();
            var mapper = mapGenerator.Generate<Foo, Bar>();
            var foo = new Foo
            {
                P1 = 123,
                P2 = "Test",
                P3 = 432.34,
                P4 = 134.3456
            };
            Console.Write(foo);
            Console.WriteLine();
            var bar = mapper.Map(foo);
            Console.Write(bar);
        }
    }
}