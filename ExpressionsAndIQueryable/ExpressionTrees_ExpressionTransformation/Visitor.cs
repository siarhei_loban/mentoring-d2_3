﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace ExpressionTrees_ExpressionTransformation
{
    public class Visitor : ExpressionVisitor
    {
        public Dictionary<string, object> Replacements { get; set; }

        public Visitor()
        {
            Replacements = new Dictionary<string, object>();
        }

        protected override Expression VisitBinary(BinaryExpression b)
        {
            var left = Visit(b.Left);
            var right = Visit(b.Right);
            if (right.NodeType == ExpressionType.Constant)
            {
                var constant = (ConstantExpression) right;
                if (constant.Type == typeof (int))
                {
                    var value = (int) constant.Value;
                    if (value == 1)
                    {
                        if (b.NodeType == ExpressionType.Add)
                        {
                            return Expression.Increment(left);
                        }
                        if (b.NodeType == ExpressionType.Subtract)
                        {
                            return Expression.Decrement(left);
                        }
                    }
                }
            }
            return base.VisitBinary(b);
        }

        protected override Expression VisitLambda<T>(Expression<T> p)
        {
            var parameters = p.Parameters.Where(x => !Replacements.ContainsKey(x.Name));
            var body = Visit(p.Body);
            var lambda = Expression.Lambda(body, parameters);
            return lambda;
        }

        protected override Expression VisitParameter(ParameterExpression p)
        {
            if (Replacements.ContainsKey(p.Name))
            {
                var value = Replacements[p.Name];
                return Expression.Constant(value);
            }
            return base.VisitParameter(p);
        }
    }
}
