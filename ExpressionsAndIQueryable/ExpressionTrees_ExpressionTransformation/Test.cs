﻿using System;
using System.Linq.Expressions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExpressionTrees_ExpressionTransformation
{
    [TestClass]
    public class Test
    {
        [TestMethod]
        public void TestMethod1()
        {
            Expression<Func<int, int, int, int>> sourceExp = (x, y, z) => (x + y + 1) + (x - 1) * z;
            new TraceExpressionVisitor().Visit(sourceExp);
            Console.Write(sourceExp.Compile()(2, 2, 2));
            Console.WriteLine();
            var visitor = new Visitor {Replacements = {["z"] = 5}};
            var resultExp = (Expression<Func<int, int, int>>) visitor.Visit(sourceExp);
            new TraceExpressionVisitor().Visit(resultExp);
            Console.Write(resultExp.Compile()(2, 2));
        }
    }
}
