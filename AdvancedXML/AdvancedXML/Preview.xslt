﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ts="http://library.by/catalog" exclude-result-prefixes="ts">
  <xsl:output method="xml" indent="yes"/>

  <xsl:template match="/">
    <rss version="2.0">
      <channel>
        <title>Books</title>
        <language>en-us</language>
        <xsl:for-each select="ts:catalog/ts:book">
          <item>
            <title><xsl:value-of select="ts:title" /></title>
            <xsl:if test="ts:isbn"><link>http://my.safaribooksonline.com//<xsl:value-of select="ts:isbn" /></link></xsl:if>
            <description><xsl:value-of select="ts:description" /></description>
            <pubDate><xsl:value-of select="ts:registration_date" /></pubDate>
            <guid><xsl:value-of select="@id" /></guid>
          </item>
        </xsl:for-each>
      </channel>
    </rss>
  </xsl:template>
</xsl:stylesheet>