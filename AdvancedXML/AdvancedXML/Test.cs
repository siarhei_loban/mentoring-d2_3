﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Xsl;

namespace AdvancedXML
{
    [TestClass]
    public class Test
    {
        /*
        Задание 1. Проверка файла обмена
        Для передачи данных о поступивших в фонды книгах используется обмен XML-файлами той же структуры, что и само хранилище (см. books.xml). 
        Библиотечная программа умеет читать эти файлы и добавлять нужную информацию в хранилище. К сожалению, если в транспортном файле нарушена 
        структура, то загрузка завершается ошибкой, с невнятным сообщением и без какой-либо диагностики, где именно нарушена структура. 
        Ваша задача:
        - Разработать XML Schema для транспортного файла. При этом чтобы делались такие проверки:
           - Тэг isbn – опциональный, но если присутствует, должен содержать корректный ISBN номер
           - Тэг genre содержит значения из заранее предопределенного списка (пусть это будут те жанры, которые присутствуют в books.xml)
           - Тэги publish_date и registration_date содержат даты в формате yyyy-mm-dd.
           - Атрибут id – является идентификатором, уникальным в рамках документа
        - Написать утилиту проверки, которая используя разработанную схему, проверяет входной файл и выдает места обнаружения ошибок (например, 
        можно выдавать id тех книг, в которых найдены ошибки, или строку и позицию тэга с ошибкой, ...).
        */
        [TestMethod]
        public void TestTask1()
        {
            var settings = new XmlReaderSettings();

            settings.Schemas.Add("http://library.by/catalog", "books.xsd");
            settings.ValidationEventHandler +=
                delegate (object sender, ValidationEventArgs e)
                {
                    Console.WriteLine(e.Message);
                };

            settings.ValidationFlags = settings.ValidationFlags | XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationType = ValidationType.Schema;

            var reader = XmlReader.Create("books.xml", settings);

            while (reader.Read()) ;
        }

        /*
        Задание 2. RSS-лента
        У библиотеки есть сайт, на который они хотели бы размещать анонсы поступлений. К сожалению, для полноценного наполнения сайта нет ресурсов, 
        поэтому решено давать RSS ленту новостей, которую строить на основе файлов каталога. 
        Ваша задача:
        - Разработать xslt преобразование из формата books.xml в RSS или Atom формат. При этом:
           - В качестве даты новости использовать дату регистрации
           - Если книга имеет жанр компьютерной литературы и указан isbn, то в качестве ссылки на полную новость давать ссылку на Online-библиотек 
           Safary Books Online в формате http://my.safaribooksonline.com// 
        - Разработать утилиту, которая принимает на вход файл формата books.xml и выдает новую ленту новостей
        */
        [TestMethod]
        public void TestTask2()
        {
            var xslt = new XslCompiledTransform();
            xslt.Load("Preview.xslt");
            xslt.Transform("books.xml", "preview.xml");
        }

        /*
        Задание 3. Отчет
        Формируем несколько HTML отчетов (особое оформление не требуется – только данные). Формирование HTML делаем с помощью XSLT.
        Необходимо сделать:
        - Отчет называется «Текущие фонды по жанрам» и содержит:
           - Шапку (заголовок) отчета с текущей датой
           - По таблице на каждый жанр, в таблице колонки:
              - Автор
              - Название
              - Дата издания
              - Дата регистрации
           - Итог по каждой таблице (общее количество книг указанного жанра)
           - Общий итог по всей библиотеке
        - Утилиту, которая принимает на вход: исходный файл с данными, файл преобразования (XSLT), файл html, куда сохранить результат.
        */
        [TestMethod]
        public void TestTask3()
        {
            var settings = new XsltSettings(false, true);
            var xslt = new XslCompiledTransform();
            xslt.Load("Report.xslt", settings, new XmlUrlResolver());
            xslt.Transform("books.xml", "report.html");
        }
    }
}
