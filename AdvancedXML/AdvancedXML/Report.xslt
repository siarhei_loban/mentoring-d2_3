﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:msxsl="urn:schemas-microsoft-com:xslt"
                xmlns:ts="http://library.by/catalog"
                xmlns:local="urn:local"
                extension-element-prefixes="msxsl"
                exclude-result-prefixes="msxsl ts local">
  <xsl:output method="html" indent="yes"/>

  <msxsl:script language="CSharp" implements-prefix="local">
    public string dateTimeNow()
    {
    return DateTime.Now.ToString("yyyy-MM-dd");
    }
  </msxsl:script>

  <xsl:template match="/">
    <html>
      <body>
        <h2>Current funds by genre</h2>
        <xsl:for-each select="ts:catalog/ts:book/ts:genre[not(.=preceding::*)]">
          <xsl:variable name="genre" select="." />
          <table>
            <caption>
              <xsl:value-of select="$genre"/>
              <xsl:text> </xsl:text>
              <xsl:value-of select="local:dateTimeNow()"/>
            </caption>
            <tr>
              <th>Author</th>
              <th>Title</th>
              <th>Publish date</th>
              <th>Registration date</th>
            </tr>
            <xsl:for-each select="/ts:catalog/ts:book[ts:genre = $genre]">
              <tr>
                <td>
                  <xsl:value-of select="ts:author"/>
                </td>
                <td>
                  <xsl:value-of select="ts:title"/>
                </td>
                <td>
                  <xsl:value-of select="ts:publish_date"/>
                </td>
                <td>
                  <xsl:value-of select="ts:registration_date"/>
                </td>
              </tr>
            </xsl:for-each>
          </table>
          <p><xsl:value-of select="$genre"/> count: <xsl:value-of select="count(/ts:catalog/ts:book[ts:genre = $genre])"/></p>
        </xsl:for-each>
        <p>Total count: <xsl:value-of select="count(/ts:catalog/ts:book)"/></p>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>