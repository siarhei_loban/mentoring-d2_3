﻿using System;
using System.Threading;
//Реализовать аналог CancelationToken'а (возможности отменить работу) для Thead'а
namespace Task3
{
    class CustomCancellationToken
    {
        private bool _canceled;
        public CustomCancellationToken(bool canceled)
        {
            _canceled = canceled;
        }
        public bool IsCancellationRequested { get { return _canceled; } }
        public void Cancel()
        {
            _canceled = true;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var token = new CustomCancellationToken(false);
            var thread = new Thread(Work);
            thread.Start(token);
            Console.WriteLine("start");
            Console.WriteLine("thread state: {0}", thread.ThreadState);
            Thread.Sleep(1000);
            token.Cancel();
            thread.Join();
            Console.WriteLine("cancel");
            Console.WriteLine("thread state: {0}", thread.ThreadState);
            Console.ReadKey();
        }

        static void Work(object obj)
        {
            try
            {
                var cancellationToken = (CustomCancellationToken)obj;

                while (true)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        Thread.CurrentThread.Abort();
                    }
                }
            }
            catch (ThreadAbortException abortException)
            {
                Console.WriteLine((string)abortException.ExceptionState);
            }
        }
    }
}
