﻿//Проверить поведение AsyncLocal, ThreadStatic, ThreadLocal, CallContext на Thread'ах/ThreadPool'е.
using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace Task2
{
    class Program
    {
        static ThreadLocal<int> _threadLocal = new ThreadLocal<int>(() => { return Thread.CurrentThread.ManagedThreadId; });
        static AsyncLocal<int> _asyncLocal = new AsyncLocal<int>();
        [ThreadStatic] static double managedThreadId;
        static void Main()
        {
            Console.WriteLine("Thread:");
            var threads = new List<Thread>();
            for (var i = 0; i < 10; i++)
            {
                var thread = new Thread(Work);
                threads.Add(thread);
                thread.Start();
            }
            foreach(var thread in threads)
            {
                thread.Join();
            }
            Console.WriteLine();
            Console.WriteLine("ThreadPool:");
            for(var i = 0; i < 10; i++)
            {
                ThreadPool.QueueUserWorkItem(Work);
            }
            Console.ReadKey();
        }

        static void Work(object state)
        {
            managedThreadId = Thread.CurrentThread.ManagedThreadId;
            _asyncLocal.Value = Thread.CurrentThread.ManagedThreadId;
            CallContext.SetData("ManagedThreadId", Thread.CurrentThread.ManagedThreadId);
            Console.WriteLine("ManagedThreadId(from ThreadLocal): {0}; ManagedThreadId(from ThreadStatic): {1}; ManagedThreadId(from AsyncLocal): {2}; ManagedThreadId(from CallContext): {3};", _threadLocal.Value, managedThreadId, _asyncLocal.Value, CallContext.GetData("ManagedThreadId"));
        }
    }
}
