﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
/*
1. Необходимо экспериментальным путём установить:
1.1 размер памяти, выделяемой под .Net поток операционной системой;
1.2. время создания потоков в зависиомсти от их количества (1-2-50-100-500).
*/
namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var sw = new Stopwatch();
            long usedMemoryBeforeCreating;
            long usedMemoryAfterCreating;
            long usedMemoryPerThread;
            long totalTimeForCreatingThreads;
            long timeForCreatingThread;
            GC.Collect();
            var numbersOfThreads = new List<int> {1, 2, 50, 100, 500};
            foreach (var numberOfThreads in numbersOfThreads)
            {
                Console.WriteLine("numbersOfThreads: {0}", numberOfThreads);
                usedMemoryBeforeCreating = GC.GetTotalMemory(false);
                Console.WriteLine("Before creating threads: {0}B.", usedMemoryBeforeCreating);
                sw.Start();
                for (var i = 0; i < numberOfThreads; i++)
                {
                    var thread = new Thread(ExecuteInForeground);
                    thread.Start(i);
                }
                sw.Stop();
                Thread.Sleep(1000);
                usedMemoryAfterCreating = GC.GetTotalMemory(false);
                Console.WriteLine("After creating threads: {0}B.", usedMemoryAfterCreating);
                usedMemoryPerThread = (usedMemoryAfterCreating - usedMemoryBeforeCreating) / numberOfThreads;
                Console.WriteLine("Memory per thread: {0}B.", usedMemoryPerThread);
                totalTimeForCreatingThreads = sw.ElapsedMilliseconds;
                Console.WriteLine("Total time: {0}ms.", totalTimeForCreatingThreads);
                timeForCreatingThread = totalTimeForCreatingThreads / numberOfThreads;
                Console.WriteLine("Time for creating 1 thread: {0}ms.", timeForCreatingThread);
                Thread.Sleep(15000);
                GC.Collect();
            }
            Console.ReadKey();
        }

        static void ExecuteInForeground(object value)
        {
            //Console.WriteLine("Thread {0} exiting...", (int) value);
            Thread.Sleep(15000);
            //Console.WriteLine("Thread {0} has exited.", (int)value);
        }
        static void MemoryTest2()
        {
            bool exit = false;
            List<object> objs = new List<object>();
            long usedMemory = 0;
            while (!exit)
            {
                try
                {
                    objs.Add(new Array[1024]);
                    usedMemory = objs.Count;
                }
                catch (InsufficientMemoryException ex)
                {
                    exit = true;
                    objs = null;
                }
                catch (OutOfMemoryException ex)
                {
                    exit = true;
                    objs = null;
                }
            }
            Console.WriteLine(String.Format("{0}kB memory available for .Net thread.", usedMemory));
        }

        static void MemoryTest1()
        {
            bool exit = false;
            List<object> objs = new List<object>();
            long usedMemory = 0;
            while (!exit)
            {
                try
                {
                    objs.Add(new Array[1024]);
                }
                catch (InsufficientMemoryException ex)
                {
                    exit = true;
                    objs = null;
                }
                catch (OutOfMemoryException ex)
                {
                    exit = true;
                    objs = null;
                }
                usedMemory = GC.GetTotalMemory(false);
            }
            GC.Collect();
            Console.WriteLine(String.Format("{0}kB memory available for .Net thread.", usedMemory / 1024));
        }
    }
}
